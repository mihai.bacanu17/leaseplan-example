Installation, Run, and Writing New Tests Instructions

1. Installation:

   Clone the project from GitLab:

   bash

   git clone https://gitlab.com/mihai.bacanu17/leaseplan-example.git

2. Run:

   Ensure Maven is locally installed.
   Run the tests with Maven:

   bash

   mvn clean verify

3. Writing New Tests:

   Open the project in a Maven-compatible IDE like IntelliJ or Eclipse.
   Add new .feature files for new test scenarios.
   Implement new tests in existing step definition classes or create new classes for them.
   Run the tests to ensure they pass successfully.

Refactoring and Updates

1. Refactoring:

   Refactored the project to use Maven exclusively for dependency management and project building.
   Updated the pom.xml file to reflect this change and ensure correct functionality.

2. Updates for Serenity Report:

   Adjusted Serenity Maven configurations in the pom.xml file to generate proper Serenity reports.
   Updated the serenity.properties file to align with the changes and specify where the reports should be generated.

3. New Methods/Functionalities:

   Added new methods and functionalities to extend the testing capabilities of the project.
   These cover specific test cases and enhance test coverage.

4. CI/CD:

   The project is now configured on GitLab with a CI/CD pipeline.
   Tests are automatically run on every push or pull request.
   Serenity reports are generated and available in the CI/CD pipeline.